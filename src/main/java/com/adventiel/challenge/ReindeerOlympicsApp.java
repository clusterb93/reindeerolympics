package com.adventiel.challenge;

import com.adventiel.challenge.model.Reindeer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.exit;

public class ReindeerOlympicsApp implements Runnable {

    private static Logger logger = Logger.getLogger(ReindeerOlympicsApp.class.getName());

    private int raceTime;
    private List<Reindeer> reindeerList;
    private String configurationFilePath;

    public ReindeerOlympicsApp(String configurationFilePath) {
        this.configurationFilePath = configurationFilePath;
    }

    public static void main(String[] args) {
        logger.log(Level.INFO, "Welcome to Reindeer Olympics");
        if (args.length != 1) {
            logger.log(Level.INFO, "Please enter the olympics configuration file path as input parameter");
            exit(1);
        }
        ReindeerOlympicsApp reindeerOlympicsApp = new ReindeerOlympicsApp(args[0]);
        reindeerOlympicsApp.run();
    }

    @Override
    public void run() {
        getRaceTime();
        loadReindeers(configurationFilePath);
        startRace();
        showWinner();
        showResults();
    }

    /**
     *
     */
    private void showResults() {
        List<Reindeer> finalResults = reindeerList.stream().sorted(Comparator.comparingInt(Reindeer::getPosition).reversed()).collect(Collectors.toList());
        for (int i = 0; i < finalResults.size(); i++) {
            Reindeer current = finalResults.get(i);
            System.out.println((i + 1) + " " + current.getName() + " finished at " + current.getPosition() + " km");
        }
    }

    /**
     * Show the winner reindeer
     */
    private void showWinner() {
        Reindeer winner = reindeerList
                .stream()
                .max(Comparator.comparing(Reindeer::getPosition))
                .orElseThrow(NoSuchElementException::new);
        System.out.println("The winner is " + winner.getName());
    }

    /**
     * Start the race!
     * - Make all the reindeers start running (they're resting before the race starts)
     * - Iterate until the end of the race
     */
    private void startRace() {
        for (Reindeer reindeer : reindeerList) {
            reindeer.changeState();
        }

        for (int i = 1; i <= raceTime; i++) {
            System.out.println("=> " + i + " seconds passed");
            reindeerList.stream().forEach(reindeer -> reindeer.performIterationOneSecond());
        }
    }

    /**
     * Load the reindeers from the input file
     *
     * @param fileName
     */
    private void loadReindeers(String fileName) {
        reindeerList = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(s -> reindeerList.add(Reindeer.getReinderFromString(s)));
        } catch (IOException e) {
            logger.log(Level.INFO, "Problem loading the race input!");
        }
    }

    /**
     * Method to get race time user input
     */
    private void getRaceTime() {
        Scanner inputScanner = new Scanner(System.in);
        do {
            System.out.println("Enter race time: ");
            while (!inputScanner.hasNextInt()) {
                System.err.println("Enter a valid race time! Int value required : ");
                inputScanner.next();
            }
            raceTime = inputScanner.nextInt();
        } while (raceTime <= 0);
        System.out.println("Race time set: " + raceTime + " seconds");
    }
}
