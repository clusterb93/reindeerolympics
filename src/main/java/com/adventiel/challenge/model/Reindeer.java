package com.adventiel.challenge.model;

public class Reindeer {
    private int speed;
    private int restingTime;
    private int flyingTime;
    private int position;
    private boolean isRunning;
    private int remainingTimeInState;
    private String name;

    public Reindeer() {
    }

    public Reindeer(String name, int speed, int flyingTime, int restingTime) {
        this.speed = speed;
        this.restingTime = restingTime;
        this.flyingTime = flyingTime;
        this.name = name;
        this.isRunning = false;
    }

    public static Reindeer getReinderFromString(String inputString) {
        if (inputString != null && !inputString.isEmpty()) {
            String[] tokens = inputString.split(" ");
            if (tokens.length > 0) {
                return new Reindeer(tokens[0], Integer.parseInt(tokens[3]), Integer.parseInt(tokens[6]), Integer.parseInt(tokens[13]));
            }
            return new Reindeer();
        }
        return null;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getRestingTime() {
        return restingTime;
    }

    public void setRestingTime(int restingTime) {
        this.restingTime = restingTime;
    }

    public int getFlyingTime() {
        return flyingTime;
    }

    public void setFlyingTime(int flyingTime) {
        this.flyingTime = flyingTime;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public int getRemainingTimeInState() {
        return remainingTimeInState;
    }

    public void setRemainingTimeInState(int remainingTimeInState) {
        this.remainingTimeInState = remainingTimeInState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to change the reindeer state
     */
    public void changeState() {
        isRunning = !isRunning;
        remainingTimeInState = isRunning ? flyingTime : restingTime;
        System.out.println("===> " + name + " changed state : " + (isRunning ? "running" : "resting"));
    }

    /**
     * Perform a one second iteration on the reindeer
     */
    public void performIterationOneSecond() {
        if (isRunning()) {
            position += speed;
            System.out.println("===> " + name + " is now at " + position + " km");
        } else {
            System.out.println("===> " + name + " is resting...");
        }
        remainingTimeInState--;
        if (remainingTimeInState == 0) {
            changeState();
        }
    }
}
