package com.adventiel.challenge.model;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReindeerTest {
    @Test
    public final void testChangeState(){
        Reindeer reindeer = new Reindeer("TestReindeer", 5, 2, 6);
        assertFalse(reindeer.isRunning());
        reindeer.changeState();
        assertTrue(reindeer.isRunning());
    }

    @Test
    public final void testIncrementPosition(){
        Reindeer reindeer = new Reindeer("TestReindeer", 10, 2, 6);
        reindeer.changeState();

        reindeer.performIterationOneSecond();
        assertEquals(10, reindeer.getPosition());
    }

    @Test
    public final void testChangeToResting(){
        Reindeer reindeer = new Reindeer("TestReindeer", 10, 2, 6);
        reindeer.changeState();

        reindeer.performIterationOneSecond();
        reindeer.performIterationOneSecond();
        assertEquals(20, reindeer.getPosition());
        assertFalse(reindeer.isRunning());
    }

    @Test
    public final void testParsingReindeer(){
        Reindeer reindeer = Reindeer.getReinderFromString("Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.");
        assertEquals("Vixen", reindeer.getName());
        assertEquals(19, reindeer.getSpeed());
        assertEquals(7, reindeer.getFlyingTime());
        assertEquals(124, reindeer.getRestingTime());
    }
}
